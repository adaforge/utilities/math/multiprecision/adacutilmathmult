/* Configuration for adacutilmathmult_tests generated by Alire */
#ifndef ADACUTILMATHMULT_TESTS_CONFIG_H
#define ADACUTILMATHMULT_TESTS_CONFIG_H

#define CRATE_VERSION "23.0.0-tst"
#define CRATE_NAME "adacutilmathmult_tests"

#define ALIRE_HOST_OS "macos"

#define ALIRE_HOST_ARCH "x86_64"

#define ALIRE_HOST_DISTRO "distro_unknown"

#define TRACE_LEVEL_INFO 1
#define TRACE_LEVEL_DETAILS 2
#define TRACE_LEVEL_DEBUG 3

#define TRACE_LEVEL  1

#define BUILD_PROFILE_RELEASE 1
#define BUILD_PROFILE_VALIDATION 2
#define BUILD_PROFILE_DEVELOPMENT 3

#define BUILD_PROFILE  3

#endif
